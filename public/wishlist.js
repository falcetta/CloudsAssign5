
const firebaseConfig = {
  apiKey: "AIzaSyAAiN7Ew20YCjoC-wf59LYwGBI-zTW9U4s",
  authDomain: "finalclouds5.firebaseapp.com",
  projectId: "finalclouds5",
  storageBucket: "finalclouds5.appspot.com",
  messagingSenderId: "598868422605",
  appId: "1:598868422605:web:eb3e25240fad3965e95e84",
  measurementId: "G-DPF8TSXJ7D"
};
// Initialize Firebase
app = firebase.initializeApp(firebaseConfig);
var wishlist;
var wishlist_name = "MyWishlist12"


user_db = firebase.firestore();

// Get wishlist from firestore or create an empty list
function readWishlist() {
  user_db.collection(wishlist_name).doc(current_user).get().then((doc) => {
    if (doc.exists) {
      console.log("RETRIEVE USER WISHLIST")
      wishlist = doc.data();
      // Create a table from the wishlist
      tableFromJson()
    }
    else {
      wishlist = {}
      console.log("Empty wishlist")
      emptyTable();
    }
  })
}

// Search filter
function filterTable(event) {
  var filter = event.target.value.toUpperCase();
  var rows = document.getElementById("movie-table").rows;

  for (var i = 0; i < rows.length; i++) {
    var firstCol = rows[i].cells[0].textContent.toUpperCase();
    var secondCol = rows[i].cells[1].textContent.toUpperCase();
    var thirdCol = rows[i].cells[2].textContent.toUpperCase();
    if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1 || thirdCol.indexOf(filter) > -1) {
      rows[i].style.display = "";
    } else {
      rows[i].style.display = "none";
    }
  }
}

document.querySelector('#search_input').addEventListener('keyup', filterTable, false);
var current_user;

// Check if the user is logged and write his name on the navbar
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    document.getElementById('logged_user').innerHTML = user.displayName;
    current_user = user.displayName;
    //Read wishlist
    readWishlist()
  }
  else {
    window.location = "/index.html"
  }
})

//Log out
function logoutUser() {
  firebase.auth().signOut().then(() => {
    window.location = "/index.html"
  }).catch((error) => {
    // An error happened.
  });
}






var keys;
function tableFromJson() {
  // Extract value from table header. 
  var col = [];
  keys = [];
  if (JSON.stringify(wishlist) == "{}") {
    emptyTable()
    return
  }
  for (const obj_key in wishlist) {
    for (var key in wishlist[obj_key]) {
      if (col.indexOf(key) === -1 && key != 'id') {
        col.push(key);
      }
    }
  }
  // Headers
  var title = col[1].charAt(0).toUpperCase() + col[1].slice(1);
  var year = col[2].charAt(0).toUpperCase() + col[2].slice(1);
  var genre = col[0].charAt(0).toUpperCase() + col[0].slice(1);
  var heart = "Remove from Wishlist"

  col = [title, year, genre, heart]


  // Create a table.
  var table = document.createElement("table");
  table.className = 'container'
  table.id = 'movie-table'

  // Create table header row using the extracted headers above.
  var tr = table.insertRow(-1);                   // table row.

  for (var i = 0; i < col.length; i++) {
    var th = document.createElement("th");      // table header.
    th.innerHTML = col[i];
    tr.appendChild(th);
  }
  count = 4000
  // add json data to the table as rows.
  for (const obj_key in wishlist) {  // Iterate over the rows  ==> db[i]
    console.log(parseInt(obj_key) + parseInt(count))
    tr = table.insertRow(-1);
    tr.id = String(parseInt(obj_key) + parseInt(count))
    var current_title = wishlist[obj_key].title
    for (var j = 0; j < col.length - 1; j++) {  // Iterate inside each row
      var tabCell = tr.insertCell(-1); // Create a cell

      tabCell.innerHTML = wishlist[obj_key][col[j].toLowerCase()];  // Insert title, year, genre
      keys[obj_key] = wishlist[obj_key].id;
    }
    //<div class="h_container">
    h_container = document.createElement('div');
    h_container.className = 'h_container';
    //<button class="heart-like-button">
    h_button = document.createElement('button');
    h_button.className = 'heart-like-button';
    h_button.id = wishlist[obj_key].id;  // TITLE AS ID FOR EACH BUTTON
    h_button.onclick = function (event) { removeFromWishlist(event.target.id) };


    h_container.appendChild(h_button) //</div></div>
    var tabCell = tr.insertCell(-1);
    tabCell.appendChild(h_container);  // Insert heart
  }

  // Now, add the newly created table with json data, to a container.
  var divShowData = document.getElementById('showData');
  divShowData.innerHTML = "";
  divShowData.appendChild(table);

}

// On blue heart click
function removeFromWishlist(id) {
  // Check if the collection alredy exists and initialize wishlist with doc.data()
  user_db.collection(wishlist_name).doc(current_user).get().then((doc) => {
    if (doc.exists) {
      console.log("WISHLIST ALREADY CREATED: UPDATE")
      wishlist = doc.data();
    }
    else {
      wishlist = {}
    }
    // Delete added key in case of error
    delete wishlist[String(id)];
    // Add the new object to the wishlist
    user_db.collection(wishlist_name).doc(current_user).set(
      wishlist)
      .then(() => {
        console.log("Document successfully written!");
        console.log(String(4000 + (id - 0)))
        document.getElementById(String(4000 + (id - 0))).style.display = 'none';
        document.getElementById(String(4000 + (id - 0))).remove();
        console.log(JSON.stringify((wishlist)));
        if (JSON.stringify(wishlist) == "{}") {
          emptyTable()
        }
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
        console.log("ERROR HERE")
      });
  })

}

function emptyTable() {
  document.getElementById('showData').style.display = 'none'
  document.getElementById('sign').style.display = '';
}












