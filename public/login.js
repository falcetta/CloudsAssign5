
const firebaseConfig = {
  apiKey: "AIzaSyAAiN7Ew20YCjoC-wf59LYwGBI-zTW9U4s",
  authDomain: "finalclouds5.firebaseapp.com",
  projectId: "finalclouds5",
  storageBucket: "finalclouds5.appspot.com",
  messagingSenderId: "598868422605",
  appId: "1:598868422605:web:eb3e25240fad3965e95e84",
  measurementId: "G-DPF8TSXJ7D"
};

// Initialize Firebase
app = firebase.initializeApp(firebaseConfig);
var wishlist;
var wishlist_name = "MyWishlist12"

var table_loaded = false, wishlist_loaded = false;

function filterTable(event) {
  if (!event) {
    var filter = "";
  } else {
    var filter = event.target.value.toUpperCase();
  }
  var rows = document.getElementById("movie-table").rows;

  for (var i = 0; i < rows.length; i++) {
    var firstCol = rows[i].cells[0].textContent.toUpperCase();
    var secondCol = rows[i].cells[1].textContent.toUpperCase();
    var thirdCol = rows[i].cells[2].textContent.toUpperCase();
    if (firstCol.indexOf(filter) > -1 || secondCol.indexOf(filter) > -1 || thirdCol.indexOf(filter) > -1) {
      if (i in wishlist) {
        rows[i].style.display = "none";
      }
      else {
        rows[i].style.display = "";
      }
    } else {
      rows[i].style.display = "none";
    }
  }
}

document.querySelector('#search_input').addEventListener('keyup', filterTable, false);
var current_user;
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    document.getElementById('logged_user').innerHTML = user.displayName;
    current_user = user.displayName;
    load();
  }
  else {
    window.location = "/index.html"
  }
})

function logoutUser() {
  firebase.auth().signOut().then(() => {
    window.location = "/index.html"
  }).catch((error) => {
    // An error happened.
  });
}

var ref = firebase.database().ref('/movies-list');
var db;
ref.on("value", function (snapshot) {
  db = snapshot.val()
  tableFromJson()

  table_loaded = true;
  if (wishlist_loaded) filterTable();

  // db[i].  title  genre year  ==> to get the string
}, function (error) {
  console.log("Error: " + error.code);
});

var keys;
function tableFromJson() {

  // Extract value from table header. 
  var col = [];
  keys = [];
  for (var i = 0; i < db.length; i++) {
    for (var key in db[i]) {
      if (col.indexOf(key) === -1 && key != 'id') {
        col.push(key);
      }
    }
  }
  var title = col[1].charAt(0).toUpperCase() + col[1].slice(1);
  var year = col[2].charAt(0).toUpperCase() + col[2].slice(1);
  var genre = col[0].charAt(0).toUpperCase() + col[0].slice(1);
  var heart = "Add to Wishlist"

  col = [title, year, genre, heart]

  // Create a table.
  var table = document.createElement("table");
  table.className = 'container'
  table.id = 'movie-table'

  // Create table header row using the extracted headers above.
  var tr = table.insertRow(-1);                   // table row.

  for (var i = 0; i < col.length; i++) {
    var th = document.createElement("th");      // table header.
    th.innerHTML = col[i];
    tr.appendChild(th);
  }
  count = 4000
  // add json data to the table as rows.
  for (var i = 0; i < db.length; i++) {  // Iterate over the rows  ==> db[i]

    tr = table.insertRow(-1);
    tr.id = String(count)
    count++
    var current_title = db[i].title
    for (var j = 0; j < col.length - 1; j++) {  // Iterate inside each row
      var tabCell = tr.insertCell(-1); // Create a cell

      tabCell.innerHTML = db[i][col[j].toLowerCase()];  // Insert title, year, genre
      keys[i] = db[i].id;
    }
    //<div class="h_container">
    h_container = document.createElement('div');
    h_container.className = 'h_container';
    //<button class="heart-like-button">
    h_button = document.createElement('button');
    h_button.className = 'heart-like-button';
    h_button.id = db[i].id;  // TITLE AS ID FOR EACH BUTTON
    h_button.onclick = function (event) { addToWishList(event.target.id) };


    h_container.appendChild(h_button) //</div></div>
    var tabCell = tr.insertCell(-1);
    tabCell.appendChild(h_container);  // Insert heart
  }

  // Now, add the newly created table with json data, to a container.
  document.getElementById("sign").style.display = 'none';
  var divShowData = document.getElementById('showData');
  divShowData.innerHTML = "";
  divShowData.appendChild(table);
  hide_wished()
}

function hide_wished() {
  for (const object_key in wishlist) {
    console.log(String(4000 + (object_key - 1)))
    document.getElementById(String(4000 + (object_key - 1))).style.display = 'none';
  }
}
user_db = firebase.firestore();

function addToWishList(id) {
  // Check if the collection alredy exists and initialize wishlist with doc.data()
  user_db.collection(wishlist_name).doc(current_user).get().then((doc) => {
    if (doc.exists) {
      console.log("WISHLIST ALREADY CREATED: UPDATE")
      wishlist = doc.data();
    }
    else {
      wishlist = {}
    }
    //Add the new row to the 
    wishlist[id] = db[id - 1];
    // Add the new object to the wishlist
    user_db.collection(wishlist_name).doc(current_user).set(
      wishlist)
      .then(() => {
        console.log("Document successfully written!");
        document.getElementById(String(4000 + (id - 1))).style.display = 'none';
        console.log(JSON.stringify((wishlist)));
      })
      .catch((error) => {
        console.error("Error writing document: ", error);
        // Delete added key in case of error
        delete wishlist[id];
      });
  })

}

function load() {
  user_db.collection(wishlist_name).doc(current_user).get().then((doc) => {
    if (doc.exists) {
      console.log("WISHLIST ALREDY EXISTS")
      wishlist = doc.data();

      wishlist_loaded = true;
      if (table_loaded) filterTable();

      console.log(JSON.stringify(wishlist));
    }
    else {
      wishlist = {};
      console.log("EMPTY WISHLIST");
    }
  })
}




